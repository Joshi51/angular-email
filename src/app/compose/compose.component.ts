import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css']
})
export class ComposeComponent implements OnInit {
  formdata: any;
  constructor() { }

  ngOnInit() {
    this.formdata = new FormGroup({
      from: new FormControl(''),
      to: new FormControl(''),
      subject: new FormControl(''),
      content: new FormControl('')
    });
  }
  sendEmail(form) {
    console.log(form);
  }
}
