import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InboxComponent} from './inbox/inbox.component';
import {MailComponent} from './mail/mail.component';
import {ComposeComponent} from './compose/compose.component';
import {DeletedComponent} from './deleted/deleted.component';
import {SentComponent} from './sent/sent.component';


const routes: Routes = [
  { path: '', redirectTo: 'inbox', pathMatch: 'full' },
  { path: 'inbox', component: InboxComponent},
  { path: 'mail/:id', component: MailComponent},
  { path: 'compose', component: ComposeComponent, outlet: 'composeModal'},
  { path: 'deleted', component: DeletedComponent},
  { path: 'sent', component: SentComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot((routes))
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
