import { Injectable } from '@angular/core';
import {AllEmailModel} from './email-type-def';
// import {InboxEmails} from './inbox-emails-data';
// import {DeletedEmails} from './deleted-emails-data';
// import {SentEmails} from './sent-emails-data';
import {AllEmails} from './all-emails-data';

@Injectable({
  providedIn: 'root'
})
export class EmailsService {
  AllEmails: Array<AllEmailModel> = [];
  getInboxEmails(): AllEmailModel[] {
    return this.AllEmails.filter(val => val.type === 'Inbox');
  }
  getDeletedEmails(): AllEmailModel[] {
    return this.AllEmails.filter(val => val.type === 'Deleted');
  }
  getSentEmails(): AllEmailModel[] {
    return this.AllEmails.filter(val => val.type === 'Sent');
  }
  getSingleEmail(id): any {
    return this.AllEmails.find(val => val.id === id);
  }
  constructor() {
    this.AllEmails = AllEmails;
  }
}
