import { Component, OnInit } from '@angular/core';
import {EmailsService} from '../emails.service';
import {EmailModel} from '../email-type-def';

@Component({
  selector: 'app-sent',
  templateUrl: './sent.component.html',
  styleUrls: ['./sent.component.css']
})
export class SentComponent implements OnInit {
  emails: Array<EmailModel> = [];
  displayedColumns: string[] = ['from', 'subject', 'content'];
  constructor(private emailServices: EmailsService) { }

  ngOnInit() {
    this.emails = this.emailServices.getSentEmails();
  }

}
