import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmailsService} from '../emails.service';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.css']
})
export class MailComponent implements OnInit {
  mailId: number;
  emailData: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private emailServices: EmailsService
  ) { }

  ngOnInit() {
    this.mailId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    this.emailData = this.emailServices.getSingleEmail(this.mailId);
  }
  deleteMail(id) {
    this.emailServices.AllEmails.forEach((val, key) => {
      if (val.id === id) {
        this.emailServices.AllEmails[key].type = 'Deleted';
      }
    });
  }
}
