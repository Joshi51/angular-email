import {AllEmailModel} from './email-type-def';
export const AllEmails: AllEmailModel[] = [
  {
    id: 1,
    content: 'First Email',
    subject: 'First Subject',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Inbox'
  },
  {
    id: 2,
    subject: 'Second Subject',
    content: 'Second Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Inbox'
  },
  {
    id: 3,
    subject: 'Third Subject',
    content: 'Third Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Inbox'
  },
  {
    id: 4,
    content: 'Fourth Email',
    subject: 'Fourth Subject',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Deleted'
  },
  {
    id: 5,
    subject: 'Fifth Subject',
    content: 'Fifth Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Deleted'
  },
  {
    id: 6,
    subject: 'Sixth Subject',
    content: 'Sixth Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Deleted'
  },
  {
    id: 7,
    content: 'Seventh Email',
    subject: 'Seventh Subject',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Sent'
  },
  {
    id: 8,
    subject: 'Eighth Subject',
    content: 'Eighth Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Sent'
  },
  {
    id: 9,
    subject: 'Ninth Subject',
    content: 'Ninth Email',
    from: {
      email: 'dumm@gmail.com',
      name: 'Sean John'
    },
    to: {
      email: 'joshi.suresh@gmail.com',
      name: 'John Sean'
    },
    type: 'Sent'
  },
];
