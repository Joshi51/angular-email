import { Component, OnInit } from '@angular/core';
import {EmailsService} from '../emails.service';
import {EmailModel} from '../email-type-def';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  emails: Array<EmailModel> = [];
  displayedColumns: string[] = ['from', 'subject', 'content'];
  constructor(private emailService: EmailsService) { }

  ngOnInit() {
    this.emails = this.emailService.getInboxEmails();
  }
}
