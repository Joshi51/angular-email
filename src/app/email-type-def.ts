export interface  EmailModel {
  id: number;
  subject: string;
  content: string;
  from: {
    email: string,
    name: string
  };
  to: {
    email: string,
    name: string
  };
}

export interface AllEmailModel {
  id: number;
  subject: string;
  content: string;
  from: {
    email: string,
    name: string
  };
  to: {
    email: string,
    name: string
  };
  type: string;
}
