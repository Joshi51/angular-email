import { Component, OnInit } from '@angular/core';
import {EmailsService} from '../emails.service';
import {EmailModel} from '../email-type-def';

@Component({
  selector: 'app-deleted',
  templateUrl: './deleted.component.html',
  styleUrls: ['./deleted.component.css']
})
export class DeletedComponent implements OnInit {
  emails: Array<EmailModel> = [];
  displayedColumns: string[] = ['from', 'subject', 'content'];
  constructor(private emailServices: EmailsService) { }

  ngOnInit() {
    this.emails = this.emailServices.getDeletedEmails();
  }

}
